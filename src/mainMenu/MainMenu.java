package mainMenu;

import com.bitacora.tl.Controller;
import com.bitacora.bl.*;
import javax.swing.JOptionPane;

public class MainMenu extends javax.swing.JDialog {

    Controller cont;
    FClientForm fClient;
    JClientForm jClient;
    TecnologyForm tecForm;
    ProjectForm projForm;
    BitacoraAcademicosForm bitacoraAcademicosForm;
    BitacoraComercialesForm bitacoraComercialesForm;
    BuscadorCliente buscadorCliente;
    ListaProyectosAcademicos listaProyectos;
    ListaClientes listaClientes;
    ListaTecnologias listaTecnologias;
    BuscadorTecnologia buscadorTecnologia;
    ListaProyectosAcademicos proyectosAcademicos;
    ListaProyectosComerciales proyectosComerciales;
    BuscadorProyecto buscadorProyecto;
    EditorUsuario editorUsuario;

    public MainMenu(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cont = new Controller();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        modUsuarioButton = new javax.swing.JMenuItem();
        changePasswordButton = new javax.swing.JMenuItem();
        logOutButton = new javax.swing.JMenuItem();
        i = new javax.swing.JMenu();
        registerProyButton = new javax.swing.JMenuItem();
        searchProyButton = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        proyectosAcademicosButton = new javax.swing.JMenuItem();
        proyectosComercialesButton = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu9 = new javax.swing.JMenu();
        registerFClientButton = new javax.swing.JMenuItem();
        registerJclientButton = new javax.swing.JMenuItem();
        searchClientButton = new javax.swing.JMenuItem();
        clientsListButton = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        registerTecButton = new javax.swing.JMenuItem();
        searchTecButton = new javax.swing.JMenuItem();
        tecListButton = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        oneProyRepButton = new javax.swing.JMenuItem();
        learningCurveButton = new javax.swing.JMenuItem();
        multipleProyRepButton = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        registrarBitacoraButton = new javax.swing.JMenu();
        bitacoraAcademicos = new javax.swing.JMenu();
        bitacoraComerciales = new javax.swing.JMenuItem();

        jMenuItem12.setText("jMenuItem12");

        jMenuItem14.setText("jMenuItem14");

        jMenuItem20.setText("jMenuItem20");

        jMenuItem2.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText(cont.GetLoogedUserName());

        jMenuBar1.setBackground(new java.awt.Color(102, 102, 102));
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));
        jMenuBar1.setPreferredSize(new java.awt.Dimension(406, 50));

        jMenu1.setText("Usuario");
        jMenu1.add(jSeparator1);

        modUsuarioButton.setText("Modificar Usuario");
        modUsuarioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modUsuarioButtonActionPerformed(evt);
            }
        });
        jMenu1.add(modUsuarioButton);

        changePasswordButton.setText("Cambiar contraseña");
        changePasswordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changePasswordButtonActionPerformed(evt);
            }
        });
        jMenu1.add(changePasswordButton);

        logOutButton.setText("Cerra Sesion");
        logOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logOutButtonActionPerformed(evt);
            }
        });
        jMenu1.add(logOutButton);

        jMenuBar1.add(jMenu1);

        i.setText("Proyectos");
        i.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                iMousePressed(evt);
            }
        });

        registerProyButton.setText("Registrar proyecto");
        registerProyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerProyButtonActionPerformed(evt);
            }
        });
        i.add(registerProyButton);

        searchProyButton.setText("Buscar proyecto");
        searchProyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchProyButtonActionPerformed(evt);
            }
        });
        i.add(searchProyButton);

        jMenu7.setText("Ver proyectos");

        proyectosAcademicosButton.setText("Academicos");
        proyectosAcademicosButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                proyectosAcademicosButtonMousePressed(evt);
            }
        });
        proyectosAcademicosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proyectosAcademicosButtonActionPerformed(evt);
            }
        });
        jMenu7.add(proyectosAcademicosButton);

        proyectosComercialesButton.setText("Comerciales");
        proyectosComercialesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                proyectosComercialesButtonMousePressed(evt);
            }
        });
        proyectosComercialesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proyectosComercialesButtonActionPerformed(evt);
            }
        });
        jMenu7.add(proyectosComercialesButton);

        i.add(jMenu7);

        jMenuBar1.add(i);

        jMenu3.setText("Clientes");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu3MousePressed(evt);
            }
        });

        jMenu9.setText("Registrar cliente");

        registerFClientButton.setText("Fisico");
        registerFClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerFClientButtonActionPerformed(evt);
            }
        });
        jMenu9.add(registerFClientButton);

        registerJclientButton.setText("Jurídico");
        registerJclientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerJclientButtonActionPerformed(evt);
            }
        });
        jMenu9.add(registerJclientButton);

        jMenu3.add(jMenu9);

        searchClientButton.setText("Buscar cliente");
        searchClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchClientButtonActionPerformed(evt);
            }
        });
        jMenu3.add(searchClientButton);

        clientsListButton.setText("Ver clientes");
        clientsListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientsListButtonActionPerformed(evt);
            }
        });
        jMenu3.add(clientsListButton);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Tecnologias");

        registerTecButton.setText("Registrar tecnología");
        registerTecButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerTecButtonActionPerformed(evt);
            }
        });
        jMenu4.add(registerTecButton);

        searchTecButton.setText("Buscar tecnología");
        searchTecButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchTecButtonActionPerformed(evt);
            }
        });
        jMenu4.add(searchTecButton);

        tecListButton.setText("Ver tecnologias");
        tecListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tecListButtonActionPerformed(evt);
            }
        });
        jMenu4.add(tecListButton);

        jMenuBar1.add(jMenu4);

        jMenu6.setText("Reporte");

        oneProyRepButton.setText("Tiempo en proyecto");
        oneProyRepButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oneProyRepButtonActionPerformed(evt);
            }
        });
        jMenu6.add(oneProyRepButton);

        learningCurveButton.setText("Curva de aprendizaje");
        learningCurveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                learningCurveButtonActionPerformed(evt);
            }
        });
        jMenu6.add(learningCurveButton);

        multipleProyRepButton.setText("Tiempo en cada proyecto");
        multipleProyRepButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multipleProyRepButtonActionPerformed(evt);
            }
        });
        jMenu6.add(multipleProyRepButton);

        jMenuBar1.add(jMenu6);

        jMenu5.setText("Bitacora");

        registrarBitacoraButton.setText("Añadir bitacora a proyecto");
        registrarBitacoraButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                registrarBitacoraButtonMousePressed(evt);
            }
        });

        bitacoraAcademicos.setText("Proyectos academicos");
        bitacoraAcademicos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                bitacoraAcademicosMousePressed(evt);
            }
        });
        registrarBitacoraButton.add(bitacoraAcademicos);

        bitacoraComerciales.setText("Proyectos comerciales");
        bitacoraComerciales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                bitacoraComercialesMousePressed(evt);
            }
        });
        registrarBitacoraButton.add(bitacoraComerciales);

        jMenu5.add(registrarBitacoraButton);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(806, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(643, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changePasswordButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changePasswordButtonActionPerformed

        cont.changePassword();
    }//GEN-LAST:event_changePasswordButtonActionPerformed

    private void registerProyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerProyButtonActionPerformed
        projForm = new ProjectForm(null, true);
        this.dispose();
        projForm.setVisible(true);
    }//GEN-LAST:event_registerProyButtonActionPerformed

    private void searchProyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchProyButtonActionPerformed
        this.dispose();
        buscadorProyecto = new BuscadorProyecto(null, true);
        buscadorProyecto.setVisible(true);
    }//GEN-LAST:event_searchProyButtonActionPerformed

    private void registerTecButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerTecButtonActionPerformed
        tecForm = new TecnologyForm(null, true);
        this.dispose();
        tecForm.setVisible(true);
    }//GEN-LAST:event_registerTecButtonActionPerformed

    private void learningCurveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_learningCurveButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_learningCurveButtonActionPerformed

    private void registerFClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerFClientButtonActionPerformed
        fClient = new FClientForm(null, true);
        this.dispose();
        fClient.setVisible(true);

    }//GEN-LAST:event_registerFClientButtonActionPerformed

    private void logOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logOutButtonActionPerformed
        cont.logOut();
        this.getParent().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_logOutButtonActionPerformed

    private void registerJclientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerJclientButtonActionPerformed
        jClient = new JClientForm(null, true);
        this.dispose();
        jClient.setVisible(true);

    }//GEN-LAST:event_registerJclientButtonActionPerformed

    private void searchClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchClientButtonActionPerformed
        this.dispose();
        buscadorCliente = new BuscadorCliente(null, true);
        buscadorCliente.setVisible(true);
    }//GEN-LAST:event_searchClientButtonActionPerformed

    private void clientsListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientsListButtonActionPerformed
        this.dispose();
        listaClientes = new ListaClientes(null, true);
        listaClientes.setVisible(true);
    }//GEN-LAST:event_clientsListButtonActionPerformed

    private void searchTecButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchTecButtonActionPerformed
        this.dispose();
        buscadorTecnologia = new BuscadorTecnologia(null, true);
        buscadorTecnologia.setVisible(true);
    }//GEN-LAST:event_searchTecButtonActionPerformed

    private void tecListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tecListButtonActionPerformed
        this.dispose();
        listaTecnologias = new ListaTecnologias(null, true);
        listaTecnologias.setVisible(true);
    }//GEN-LAST:event_tecListButtonActionPerformed

    private void oneProyRepButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oneProyRepButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_oneProyRepButtonActionPerformed

    private void multipleProyRepButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multipleProyRepButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_multipleProyRepButtonActionPerformed

    private void registrarBitacoraButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_registrarBitacoraButtonMousePressed

    }//GEN-LAST:event_registrarBitacoraButtonMousePressed

    private void bitacoraAcademicosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bitacoraAcademicosMousePressed
        bitacoraAcademicosForm = new BitacoraAcademicosForm(null, true);
        this.dispose();
        bitacoraAcademicosForm.setVisible(true);

    }//GEN-LAST:event_bitacoraAcademicosMousePressed

    private void bitacoraComercialesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bitacoraComercialesMousePressed
        bitacoraComercialesForm = new BitacoraComercialesForm(null, true);
        this.dispose();
        bitacoraComercialesForm.setVisible(true);

    }//GEN-LAST:event_bitacoraComercialesMousePressed

    private void jMenu3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MousePressed

    }//GEN-LAST:event_jMenu3MousePressed

    private void proyectosComercialesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proyectosComercialesButtonActionPerformed
        this.dispose();
        proyectosComerciales = new ListaProyectosComerciales(null, true);
        proyectosComerciales.setVisible(true);
    }//GEN-LAST:event_proyectosComercialesButtonActionPerformed

    private void iMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_iMousePressed

    }//GEN-LAST:event_iMousePressed

    private void proyectosComercialesButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_proyectosComercialesButtonMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_proyectosComercialesButtonMousePressed

    private void proyectosAcademicosButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_proyectosAcademicosButtonMousePressed
        this.dispose();
        proyectosAcademicos = new ListaProyectosAcademicos(null, true);
        proyectosAcademicos.setVisible(true);
    }//GEN-LAST:event_proyectosAcademicosButtonMousePressed

    private void proyectosAcademicosButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proyectosAcademicosButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_proyectosAcademicosButtonActionPerformed

    private void modUsuarioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modUsuarioButtonActionPerformed
        this.dispose();
        Usuario usuario = cont.GetLoggedUser();
        editorUsuario = new EditorUsuario(null, true, usuario);
        editorUsuario.setVisible(true);
    }//GEN-LAST:event_modUsuarioButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu bitacoraAcademicos;
    private javax.swing.JMenuItem bitacoraComerciales;
    private javax.swing.JMenuItem changePasswordButton;
    private javax.swing.JMenuItem clientsListButton;
    private javax.swing.JMenu i;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem learningCurveButton;
    private javax.swing.JMenuItem logOutButton;
    private javax.swing.JMenuItem modUsuarioButton;
    private javax.swing.JMenuItem multipleProyRepButton;
    private javax.swing.JMenuItem oneProyRepButton;
    private javax.swing.JMenuItem proyectosAcademicosButton;
    private javax.swing.JMenuItem proyectosComercialesButton;
    private javax.swing.JMenuItem registerFClientButton;
    private javax.swing.JMenuItem registerJclientButton;
    private javax.swing.JMenuItem registerProyButton;
    private javax.swing.JMenuItem registerTecButton;
    private javax.swing.JMenu registrarBitacoraButton;
    private javax.swing.JMenuItem searchClientButton;
    private javax.swing.JMenuItem searchProyButton;
    private javax.swing.JMenuItem searchTecButton;
    private javax.swing.JMenuItem tecListButton;
    // End of variables declaration//GEN-END:variables
}
