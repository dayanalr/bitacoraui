package mainMenu;

import com.bitacora.bl.Bitacora;
import javax.swing.JOptionPane;
import com.bitacora.tl.Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JComboBox;

public class ProjectForm extends javax.swing.JDialog {

    Date date;
    Controller cont = new Controller();
    JClientForm jClient;
    FClientForm fClient;
    TecnologyForm tecForm;
    BitacoraAcademicosForm bitacoraForm;
    MainMenu menu;

    String projectType = "";
    AllEntriesForm allEntries;

    public ProjectForm(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cont = new Controller();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        descrField = new javax.swing.JTextField();
        startDate = new org.jdesktop.swingx.JXDatePicker();
        endDate = new org.jdesktop.swingx.JXDatePicker();
        jScrollPane1 = new javax.swing.JScrollPane();
        clientsList = new javax.swing.JList<>();
        cancelButton = new javax.swing.JButton();
        registerButton = new javax.swing.JButton();
        newClientOption = new javax.swing.JLabel();
        refreshClients = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        projectTypeOption = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tecnologiesList = new javax.swing.JList<>();
        newTecnologieOption = new javax.swing.JLabel();
        refreshTechnologies = new javax.swing.JLabel();

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Registrar proyecto");

        jLabel3.setText("Nombre");

        jLabel4.setText("Descripcion");

        jLabel5.setText("Fecha de Inicio");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel6.setText("Clientes");

        jLabel7.setText("Fecha de finalizacion");

        nameField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameFieldActionPerformed(evt);
            }
        });

        descrField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descrFieldActionPerformed(evt);
            }
        });

        startDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startDateActionPerformed(evt);
            }
        });

        endDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                endDateActionPerformed(evt);
            }
        });

        clientsList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = cont.getAllClients();
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(clientsList);

        cancelButton.setText("Cancelar");

        registerButton.setText("Registrar");
        registerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerButtonActionPerformed(evt);
            }
        });

        newClientOption.setForeground(new java.awt.Color(0, 153, 153));
        newClientOption.setText("[+] Nuevo Cliente");
        newClientOption.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                newClientOptionMousePressed(evt);
            }
        });

        refreshClients.setForeground(new java.awt.Color(0, 153, 153));
        refreshClients.setText("Refrescar lista");
        refreshClients.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                refreshClientsMousePressed(evt);
            }
        });

        jLabel8.setText("Tipo");

        projectTypeOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Proyecto academico", "Proyecto comercial"}));
        projectTypeOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                projectTypeOptionActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel9.setText("Tecnologias");

        //tecnologiesList.setSelectionMode(listSelectionModel.MULTIPLE_INTERVAL_SELECTION );
        tecnologiesList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = cont.getAllTecnologies();
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }

        });
        jScrollPane2.setViewportView(tecnologiesList);

        newTecnologieOption.setForeground(new java.awt.Color(0, 153, 153));
        newTecnologieOption.setText("[+] Nueva Tecnologia");
        newTecnologieOption.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                newTecnologieOptionMousePressed(evt);
            }
        });

        refreshTechnologies.setForeground(new java.awt.Color(0, 153, 153));
        refreshTechnologies.setText("Refrescar lista");
        refreshTechnologies.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                refreshTechnologiesMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(startDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(endDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(58, 58, 58)
                        .addComponent(projectTypeOption, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(newTecnologieOption)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(refreshTechnologies))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(newClientOption)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(refreshClients))
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cancelButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(registerButton))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(38, 38, 38)
                        .addComponent(nameField))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(descrField)))
                .addGap(151, 151, 151))
            .addGroup(layout.createSequentialGroup()
                .addGap(232, 232, 232)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel1)
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(projectTypeOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(descrField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(endDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newClientOption)
                    .addComponent(refreshClients))
                .addGap(26, 26, 26)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newTecnologieOption)
                    .addComponent(refreshTechnologies))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(registerButton))
                .addGap(45, 45, 45))
        );

        projectTypeOption.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                projectTypeOption = new javax.swing.JComboBox<>();

                projectTypeOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Proyecto academico", "Proyecto comercial"}));

                projectTypeOption.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        projectTypeOptionActionPerformed(evt);
                    }
                });
                projectTypeOption = (JComboBox) event.getSource();

                Object selected = projectTypeOption.getSelectedItem();
                if (selected.toString().equals("Proyecto academico")) {
                    clientsList.setEnabled(false);
                }

            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void endDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_endDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_endDateActionPerformed

    private void newClientOptionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newClientOptionMousePressed
        Object[] clientOptions = {"Cliente Fisico", "Cliente Juridico"};
        Object selectedValue = JOptionPane.showInputDialog(null,
                "Seleccione el tipo de cliente", "",
                JOptionPane.INFORMATION_MESSAGE, null,
                clientOptions, clientOptions[0]);
        if (selectedValue.equals("Cliente Fisico")) {
            fClient = new FClientForm(null, true);
            fClient.setVisible(true);
        } else {
            jClient = new JClientForm(null, true);
            jClient.setVisible(true);
        }
    }//GEN-LAST:event_newClientOptionMousePressed

    private void refreshClientsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refreshClientsMousePressed
        clientsList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = cont.getAllClients();

            public int getSize() {
                return strings.length;
            }

            public String getElementAt(int i) {
                return strings[i];
            }
        });

    }//GEN-LAST:event_refreshClientsMousePressed

    private void registerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerButtonActionPerformed
        String nombre = nameField.getText();
        String descripcion = descrField.getText();
        DateFormat sysDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String fechaInicio = sysDate.format(startDate.getDate()).toString();
        String fechaFin = sysDate.format(endDate.getDate()).toString();
        int[] indicesTecnologias = tecnologiesList.getSelectedIndices();
        Object tipoProyecto = projectTypeOption.getSelectedItem();

        if (tipoProyecto.equals("Proyecto academico")) {
            cont.createAcademicProject(nombre, descripcion, fechaInicio, fechaFin, indicesTecnologias);
            this.dispose();
            menu = new MainMenu(null,true);
            menu.setVisible(true);
            
        } else {
            int indiceCliente = clientsList.getSelectedIndex();
            cont.createComercialProject(nombre, descripcion, fechaInicio, fechaFin, clientsList.getSelectedIndex(), tecnologiesList.getSelectedIndices());
            this.dispose();
            menu = new MainMenu(null,true);
            menu.setVisible(true);
        }
    }//GEN-LAST:event_registerButtonActionPerformed


    private void nameFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameFieldActionPerformed

    private void descrFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descrFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_descrFieldActionPerformed

    private void projectTypeOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_projectTypeOptionActionPerformed

    }//GEN-LAST:event_projectTypeOptionActionPerformed

    private void newTecnologieOptionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newTecnologieOptionMousePressed
        tecForm = new TecnologyForm(null, true);
        this.dispose();
        tecForm.setVisible(true);
    }//GEN-LAST:event_newTecnologieOptionMousePressed

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed

    }//GEN-LAST:event_formMousePressed

    private void refreshTechnologiesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refreshTechnologiesMousePressed
        tecnologiesList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = cont.getAllTecnologies();

            public int getSize() {
                return strings.length;
            }

            public String getElementAt(int i) {
                return strings[i];
            }
        });

    }//GEN-LAST:event_refreshTechnologiesMousePressed

    private void startDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startDateActionPerformed

    }//GEN-LAST:event_startDateActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JList<String> clientsList;
    private javax.swing.JTextField descrField;
    private org.jdesktop.swingx.JXDatePicker endDate;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField nameField;
    private javax.swing.JLabel newClientOption;
    private javax.swing.JLabel newTecnologieOption;
    private javax.swing.JComboBox<String> projectTypeOption;
    private javax.swing.JLabel refreshClients;
    private javax.swing.JLabel refreshTechnologies;
    private javax.swing.JButton registerButton;
    private org.jdesktop.swingx.JXDatePicker startDate;
    private javax.swing.JList<String> tecnologiesList;
    // End of variables declaration//GEN-END:variables

}
